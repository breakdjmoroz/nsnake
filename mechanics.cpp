#include <cstdlib>
#include <ncurses.h>

#include "mechanics.hpp"

void printGameOverMessage(const Scene& scene)
{
  mvwprintw(scene.field, DEFAULT_PLAYER_Y - 2, DEFAULT_PLAYER_X - 4, "%s", DEFAULT_GAME_OVER_MESSAGE);
  mvwprintw(scene.field, DEFAULT_PLAYER_Y - 1, DEFAULT_PLAYER_X - 5, "%s", DEFAULT_PLAY_AGAIN_MESSAGE); 
  mvwprintw(scene.field, DEFAULT_PLAYER_Y, DEFAULT_PLAYER_X - 3, "%s", DEFAULT_CHOOSING_HINT_MESSAGE);
}

void clearGameOverMessage(const Scene& scene)
{
  mvwprintw(scene.field, DEFAULT_PLAYER_Y - 2, DEFAULT_PLAYER_X - 4, "%s", "         ");
  mvwprintw(scene.field, DEFAULT_PLAYER_Y - 1, DEFAULT_PLAYER_X - 5, "%s", "           ");
  mvwprintw(scene.field, DEFAULT_PLAYER_Y, DEFAULT_PLAYER_X - 3, "%s", "       ");
}

void movePlayer(const unsigned int direction, const unsigned int prevDirection, Player& player)
{
  switch(direction)
  {
    case KEY_UP:
      player.goUp();
      break;
    case KEY_LEFT:
      player.goLeft();
      break;
    case KEY_DOWN:
      player.goDown();
      break;
    case KEY_RIGHT:
      player.goRight();
      break;
    default:
      switch(prevDirection)
      {
        case KEY_UP:
          player.goUp();
          break;
        case KEY_LEFT:
          player.goLeft();
          break;
        case KEY_DOWN:
          player.goDown();
          break;
        case KEY_RIGHT:
          player.goRight();
          break;
      }
      break;
  }
}

bool hasPlayerCatchAnApple(const Player& player, const Apple& apple)
{
  return player.isHeadAt(apple.coordinates.y, apple.coordinates.x);
}

void showApple(const Scene& scene, const Apple& apple)
{
  wattron(scene.field, COLOR_PAIR(apple.color));
  mvwaddch(scene.field, apple.coordinates.y, apple.coordinates.x, apple.symbol);
  updateScene(scene);
  wattrset(scene.field, A_NORMAL);
}

void cleanApple(const Scene& scene, const Apple& apple)
{
  mvwaddch(scene.field, apple.coordinates.y, apple.coordinates.x, ' ');
  updateScene(scene);
}

Apple::Coordinates generateCoordinates(const Scene& scene)
{
  Apple apple = {};

  unsigned short fieldSize = scene.height * scene.width;

  unsigned short appleCoordinates = static_cast< unsigned short >(rand() % fieldSize);

  bool inSearch = true;
  while (inSearch)
  {
    if (inSnake(scene, appleCoordinates) || inBorders(scene, appleCoordinates))
    {
      appleCoordinates = static_cast< unsigned short >(rand() % fieldSize);
    }
    else
    {
      inSearch = false;
    }
  }

  apple.coordinates.y = appleCoordinates / scene.width;
  apple.coordinates.x = appleCoordinates % scene.width;

  return apple.coordinates;
}

bool inSnake(const Scene& scene, const unsigned short appleCoordinates)
{
  return scene.playerStatus[appleCoordinates / BYTE] & (1 << ((BYTE - 1) - (appleCoordinates % BYTE)));
}

bool inBorders(const Scene& scene, const unsigned short appleCoordinates)
{
  return appleCoordinates % scene.width == 0 ||
    appleCoordinates % scene.width == (scene.width - 1) ||
    appleCoordinates < scene.width || 
    appleCoordinates > scene.width * (scene.height - 1);
}

