#include "score.hpp"

ScoreWin createScore()
{
  unsigned short score = 0;

  WINDOW* window = newwin(DEFAULT_SCORE_HEIGHT, DEFAULT_SCORE_WIDTH, DEFAULT_SCORE_Y, DEFAULT_SCORE_X);

  wprintw(window, "%s%5i", "SCORE:", score);
  wrefresh(window);

  return ScoreWin {window, score};
}

void updateScore(const ScoreWin& scoreWin)
{
  mvwprintw(scoreWin.window, 0, 6, "%5i", scoreWin.score);
  wrefresh(scoreWin.window);
}

void destroyScore(const ScoreWin& scoreWin)
{
 delwin(scoreWin.window); 
}

void increaseScore(ScoreWin& scoreWin)
{
 ++scoreWin.score; 
 updateScore(scoreWin);
}

void resetScore(ScoreWin& scoreWin)
{
  scoreWin.score = 0;
  mvwprintw(scoreWin.window, 0, 6, "%5i", scoreWin.score);
  wrefresh(scoreWin.window);
}
