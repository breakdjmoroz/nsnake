#ifndef APPLE
#define APPLE

struct Apple
{
  struct Coordinates
  {
    short y;
    short x;
  };

  Coordinates coordinates;
  char symbol;
  char color;
};

#endif

