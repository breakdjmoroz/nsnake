#include "menu.hpp"

Menu createMenu(const char color, const char subColor)
{
  WINDOW* window = newwin(DEFAULT_MENU_HEIGHT, DEFAULT_MENU_WIDTH,
      DEFAULT_MENU_Y, DEFAULT_MENU_X);

  wattron(window, COLOR_PAIR(color));
  wprintw(window, " %s ", MENU_STR);
  wattroff(window, COLOR_PAIR(color));

  keypad(window, TRUE);

  wrefresh(window);

  return Menu {window, {MENU_ITEMS::INFORM, MENU_ITEMS::EXIT}, color, subColor};
}

void updateMenu(const Menu& menu)
{
  wrefresh(menu.window);
}

void destroyMenu(const Menu& menu)
{
  delwin(menu.window);
}

void openMenu(const Menu& menu)
{
  wattron(menu.window, COLOR_PAIR(menu.subColorScheme));

  for (char i = 0; i < DEFAULT_MENU_HEIGHT - 1; ++i)
  {
    switch (menu.choices[i])
    { 
      case MENU_ITEMS::INFORM:
        mvwprintw(menu.window, i + 1, 0, "%-6s", INFO_STR); 
        break;
      case MENU_ITEMS::EXIT:
        mvwprintw(menu.window, i + 1, 0, "%-6s", EXIT_STR);
        break;
    }
  }

  wattroff(menu.window, COLOR_PAIR(menu.subColorScheme));

  updateMenu(menu);
}

void closeMenu(const Menu& menu)
{
  for (char i = 1; i < DEFAULT_MENU_HEIGHT; ++i)
  {
    mvwprintw(menu.window, i, 0, "      "); 
  }

  updateMenu(menu);
}

MENU_ITEMS select(const Menu& menu)
{
  MENU_ITEMS choice = MENU_ITEMS::ZERO;
  unsigned int key = '\0';
  unsigned int currentItem = 1;
  
  bool isMenuOpen = true;

  mvwchgat(menu.window, currentItem, 0, -1, A_BLINK, menu.subColorScheme, NULL);

  while ((choice == MENU_ITEMS::ZERO) && isMenuOpen)
  {
    key = wgetch(menu.window);

    switch (key)
    {
      case KEY_UP:
        if (currentItem > 1)
        {
          mvwchgat(menu.window, currentItem, 0, -1, A_NORMAL, menu.subColorScheme, NULL);
          --currentItem;
          mvwchgat(menu.window, currentItem, 0, -1, A_BLINK, menu.subColorScheme, NULL);
        }
        break;
      case KEY_DOWN:
        if (currentItem < DEFAULT_MENU_HEIGHT - 1)
        {
          mvwchgat(menu.window, currentItem, 0, -1, A_NORMAL, menu.subColorScheme, NULL);
          ++currentItem;
          mvwchgat(menu.window, currentItem, 0, -1, A_BLINK, menu.subColorScheme, NULL);
        }
        break;
      case ENTER:
        choice = menu.choices[currentItem - 1];
        break;
      case DEFAULT_MENU_CHARACTER:
        isMenuOpen = false;
        break;
    } 

    updateMenu(menu);
  }

  return choice;
}

