#include "infoPage.hpp"

InfoPage createInfo(const char color)
{
	WINDOW* window = newwin(DEFAULT_INFO_HEIGHT, DEFAULT_INFO_WIDTH,
			DEFAULT_INFO_Y, DEFAULT_INFO_X);

	wrefresh(window);

	return InfoPage {window, color};
}

void updateInfo(const InfoPage& info)
{
	wrefresh(info.window);
}

void destroyInfo(const InfoPage& info)
{
	delwin(info.window);
}

void showInfo(const InfoPage& info)
{
	wborder(info.window, '|', '|', '=', '=', '+', '+', '+', '+');

	for (int i = 0; i < SIZE_OF_MESSAGE; ++i)
	{
		mvwprintw(info.window, i + 1, 1, "%s", INFO_MESSAGE[i]);
	}

	wrefresh(info.window);

	int ch = '\0';

	while (ch != 'q')
	{
		ch = wgetch(info.window);
	}

	clearInfo(info);
}

void clearInfo(const InfoPage& info)
{
	wborder(info.window, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

	for (int i = 0; i < SIZE_OF_MESSAGE; ++i)
	{
		mvwprintw(info.window, i + 1, 1, "%s", INFO_MESSAGE_CLEAR[i]);
	}

	wrefresh(info.window);
}

