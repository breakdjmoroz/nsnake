#ifndef SCENE
#define SCENE

#include <ncurses.h>

const unsigned short DEFAULT_HEIGHT = 15;
const unsigned short DEFAULT_WIDTH = 38;
const unsigned short DEFAULT_START_Y = 1;
const unsigned short DEFAULT_START_X = 7;

const char BYTE = 8;

struct Scene
{
  WINDOW* field;

  char* playerStatus;

  unsigned short height;
  unsigned short width;
};

Scene createScene(const unsigned short = DEFAULT_HEIGHT, const unsigned short = DEFAULT_WIDTH);
void updateScene(const Scene&);
void resetScene(const Scene&);
void destroyScene(const Scene&);

#endif

