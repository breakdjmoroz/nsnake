#ifndef COLOR_SCHEMES
#define COLOR_SCHEMES

#include "ncurses.h"

const int COLOR_GRAY = 8;

const char SNAKE_COLOR_SCHEME = 1;
const char APPLE_COLOR_SCHEME = 2;
const char MAIN_MENU_COLOR_SCHEME = 3;
const char SUB_MENU_COLOR_SCHEME = 4;
const char INFO_PAGE_COLOR_SCHEME = 5;

#endif

