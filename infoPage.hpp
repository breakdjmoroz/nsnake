#ifndef INFO
#define INFO

#include "ncurses.h"

#include "scene.hpp"

const char SIZE_OF_MESSAGE = 13;

const char* const INFO_MESSAGE [SIZE_OF_MESSAGE] = {
"   THIS IS INFORMATION PAGE",
"        OF THIS GAME",
"",
" TO CONTROL YOUR SNAKE USE:",
"",
"          (key up)",
"              |",
"(key left) --- --- (key right)",
"              |",
"          (key down)",
"",
"TO OPEN THE MENU PRESS 'm' KEY",
"    TO CLOSE INFO TYPE 'q'"
};

const char* const INFO_MESSAGE_CLEAR [SIZE_OF_MESSAGE] = {
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
"                               ",
};


const char DEFAULT_INFO_HEIGHT = 15;
const char DEFAULT_INFO_WIDTH = 32;
const char DEFAULT_INFO_Y = DEFAULT_START_Y;
const char DEFAULT_INFO_X = DEFAULT_START_X + DEFAULT_WIDTH + 2;

struct InfoPage
{
	WINDOW* window;
	char colorScheme;
};

InfoPage createInfo(const char);
void updateInfo(const InfoPage&);
void destroyInfo(const InfoPage&);

void showInfo(const InfoPage&);
void clearInfo(const InfoPage&);

#endif

