#include <cmath>

#include "scene.hpp"

Scene createScene(const unsigned short height, const unsigned short width)
{
  WINDOW* field = newwin(height, width, static_cast< int >(DEFAULT_START_Y), static_cast< int >(DEFAULT_START_X));
  box(field, 0, 0);

  wrefresh(field);

  unsigned short fieldSize = std::ceil(static_cast< float >(height * width) / static_cast< float > (BYTE));

  char* playerStatus = new char[fieldSize]{};

  return Scene {field, playerStatus, height, width};
}

void updateScene(const Scene& scene)
{
  wrefresh(scene.field);
}

void resetScene(const Scene& scene)
{
  box(scene.field, 0, 0);
  wrefresh(scene.field);
}

void destroyScene(const Scene& scene)
{
  delete [] scene.playerStatus;
  wborder(scene.field, ' ', ' ', ' ',' ',' ',' ',' ',' ');
  wrefresh(scene.field);
  delwin(scene.field);
}

