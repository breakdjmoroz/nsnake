#include <ncurses.h>
 
#include "game.hpp"

int main()
{
  initscr();
  curs_set(0);
  cbreak();
  noecho();
  halfdelay(DEFAULT_SPEED);

  start_color();

  init_color(COLOR_GRAY, 500, 500, 500);

  init_pair(SNAKE_COLOR_SCHEME, COLOR_GREEN, COLOR_BLACK);
  init_pair(APPLE_COLOR_SCHEME, COLOR_YELLOW, COLOR_BLACK);
  init_pair(MAIN_MENU_COLOR_SCHEME, COLOR_BLACK, COLOR_WHITE);
  init_pair(SUB_MENU_COLOR_SCHEME, COLOR_BLACK, COLOR_GRAY);
  init_pair(INFO_PAGE_COLOR_SCHEME, COLOR_GRAY, COLOR_BLACK);

  Scene scene = createScene();

  keypad(scene.field, TRUE);

  ScoreWin scoreWin = createScore();
  Menu menu = createMenu(MAIN_MENU_COLOR_SCHEME, SUB_MENU_COLOR_SCHEME);
  InfoPage info = createInfo(INFO_PAGE_COLOR_SCHEME);

  Apple apple {{0, 0}, 'o', APPLE_COLOR_SCHEME};
  apple.coordinates = generateCoordinates(scene);
  showApple(scene, apple);

  Player player('0', SNAKE_COLOR_SCHEME);
  player.show(scene);

  unsigned int direction = '\0'; 
  unsigned int prevDirection = '\0';

  bool isPlaying = true;

  while(isPlaying)
  {
    direction = wgetch(scene.field);

    if (direction == DEFAULT_MENU_CHARACTER)
    {
      openMenu(menu);
      MENU_ITEMS choice = select(menu);

      switch (choice)
      {
        case MENU_ITEMS::INFORM:
          showInfo(info);
          break;
        case MENU_ITEMS::EXIT:
          isPlaying = false;
          break;
      }

      closeMenu(menu);
    }
    else 
    {
      player.clean(scene);

      movePlayer(direction, prevDirection, player);

      if (hasPlayerCatchAnApple(player, apple))
      {
        player.biteAnApple();

        increaseScore(scoreWin);

        player.updateStatus(scene);

        apple.coordinates = generateCoordinates(scene);
        showApple(scene, apple);

        player.cleanStatus(scene);
      }
      else if (player.hasCrashed())
      {
        cleanApple(scene, apple);
        player.reset(scene);
        resetScene(scene);
        resetScore(scoreWin);

        printGameOverMessage(scene);

        unsigned ch = '\0';

        while (ch != 'y' && ch != 'n')
        {
          ch = wgetch(scene.field);

          if (ch == 'y')
          {
            clearGameOverMessage(scene);

            direction = '\0';
            prevDirection = '\0';

            apple.coordinates = generateCoordinates(scene);
            showApple(scene, apple);
          }
          else if (ch == 'n')
          {
            isPlaying = false;
          }
        }
      }

      player.show(scene);

      if (direction == KEY_UP || direction == KEY_LEFT || direction == KEY_DOWN || direction == KEY_RIGHT)
      prevDirection = direction;
    }
  }

  destroyScene(scene);
  destroyScore(scoreWin);
  destroyInfo(info);
  endwin();

  return 0;
}

