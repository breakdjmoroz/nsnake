#ifndef MECHANICS
#define MECHANICS 

#include "scene.hpp"
#include "player.hpp"
#include "apple.hpp"

const char* const DEFAULT_GAME_OVER_MESSAGE = "You lose!";
const char* const DEFAULT_PLAY_AGAIN_MESSAGE = "Try again?";
const char* const DEFAULT_CHOOSING_HINT_MESSAGE = "[y / n]";

void printGameOverMessage(const Scene&);
void clearGameOverMessage(const Scene&);

void movePlayer(const unsigned int, const unsigned int, Player&);

bool hasPlayerCatchAnApple(const Player&, const Apple&);

void showApple(const Scene&, const Apple&);
void cleanApple(const Scene&, const Apple&);

Apple::Coordinates generateCoordinates(const Scene&);
bool inSnake(const Scene&, const unsigned short);
bool inBorders(const Scene&, const unsigned short);

#endif

