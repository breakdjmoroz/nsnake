#include <cmath>
#include <ncurses.h>

#include "player.hpp"

Player::Player(const char body, const char color):
  bodyChar(body),
  colorScheme(color),
  bodySize(0)
{};

void Player::move()
{
  for (unsigned short i = bodySize; i > 0; --i)
  {
    coordinates[i] = coordinates[i - 1];
  }
}

void Player::goUp()
{
  move();
  --coordinates[0].y;
}
void Player::goDown()
{
  move();
  ++coordinates[0].y;
}
void Player::goRight()
{
  move();
  ++coordinates[0].x;
}
void Player::goLeft()
{
  move();
  --coordinates[0].x;
}

void Player::show(const Scene& scene) const
{
  wattron(scene.field, COLOR_PAIR(colorScheme));
  mvwaddch(scene.field, coordinates[0].y, coordinates[0].x, bodyChar);
  updateScene(scene);
  wattrset(scene.field, A_NORMAL);
}

void Player::clean(const Scene& scene) const
{
  mvwaddch(scene.field, coordinates[bodySize].y, coordinates[bodySize].x, ' ');
  updateScene(scene);
}

void Player::erase(const Scene& scene) const
{
  for (unsigned short i = 0; i <= bodySize; ++i)
  {
    mvwaddch(scene.field, coordinates[i].y, coordinates[i].x, ' ');
  }
  updateScene(scene);
}

bool Player::isHeadAt(const char y, const char x) const
{
  return coordinates[0].y == y && coordinates[0].x == x;
}

void Player::biteAnApple()
{
  increaseSize();
  addTail();
}

void Player::increaseSize()
{
  ++bodySize;
}

void Player::addTail()
{
  coordinates[bodySize] = coordinates[bodySize - 1];
}

bool Player::inItself() const
{
  bool result = false;

  for (unsigned short i = bodySize; i > 0 && !result; --i)
  {
   result = coordinates[0] == coordinates[i];
  }

  return result;
}

bool Player::hasCrashed() const
{
  return coordinates[0].y == (DEFAULT_HEIGHT - 1) ||
    coordinates[0].x == (DEFAULT_WIDTH - 1) ||
    coordinates[0].y == 0 ||
    coordinates[0].x == 0 ||
    inItself();
}


bool operator==(const Player::BodyNode& first, const Player::BodyNode& second)
{
  return (first.y == second.y) && (first.x == second.x);
}

void Player::updateStatus(Scene& scene) const
{
  for (unsigned short i = 0; i <= bodySize; ++i)
  {
    char y = coordinates[i].y;
    char x = coordinates[i].x;

    unsigned short bitNumber = (y * scene.width) + x;

    char shiftDegree = (BYTE - 1) - (bitNumber % BYTE);

    scene.playerStatus[bitNumber / BYTE] |= (1 << shiftDegree);
  }
}

void Player::cleanStatus(Scene& scene) const
{
  unsigned short size = std::ceil(static_cast< float >(scene.height * scene.width) / static_cast< float >(BYTE));
  for (unsigned short i = 0; i < size; ++i)
  {
    if (scene.playerStatus[i])
    {
      scene.playerStatus[i] *= 0;
    }
  } 
}

void Player::reset(const Scene& scene)
{
  erase(scene);
  coordinates[0] = {DEFAULT_PLAYER_Y, DEFAULT_PLAYER_X};
  bodySize = 0;
}

