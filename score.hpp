#ifndef SCORE
#define SCORE

#include "ncurses.h"
#include "scene.hpp"

const char DEFAULT_SCORE_HEIGHT = 1;
const char DEFAULT_SCORE_WIDTH = 11;
const char DEFAULT_SCORE_Y = DEFAULT_START_Y + DEFAULT_HEIGHT;
const char DEFAULT_SCORE_X = DEFAULT_START_X + DEFAULT_WIDTH - DEFAULT_SCORE_WIDTH;

struct ScoreWin
{
  WINDOW* window;
  unsigned short score;
};

ScoreWin createScore();
void updateScore(const ScoreWin&);
void destroyScore(const ScoreWin&);
void increaseScore(ScoreWin&);
void resetScore(ScoreWin&);

#endif

