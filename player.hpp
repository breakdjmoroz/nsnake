#ifndef PLAYER
#define PLAYER

#include "scene.hpp"

const unsigned short DEFAULT_PLAYER_Y = DEFAULT_HEIGHT / 2;
const unsigned short DEFAULT_PLAYER_X = DEFAULT_WIDTH / 2;
const char DEFAULT_SPEED = 2;

class Player
{
public:

  Player(const char, const char);
  Player(const Player&) = default;
  Player(Player&&) = default;

  Player& operator=(const Player&) = default;
  Player& operator=(Player&&) = default;

  ~Player() = default;

  void goUp();
  void goDown();
  void goRight();
  void goLeft();

  void show(const Scene&) const;
  void clean(const Scene&) const;

  bool isHeadAt(const char, const char) const;

  void reset(const Scene&);

  void biteAnApple();

  bool hasCrashed() const;

  void updateStatus(Scene&) const;
  void cleanStatus(Scene&) const;

  struct alignas(short) BodyNode
  {
    char y;
    char x;
  };

private:

  void move();

  void erase(const Scene& scene) const;

  void increaseSize();
  void addTail();

  bool inItself() const;

  BodyNode coordinates [(DEFAULT_HEIGHT - 2) * (DEFAULT_WIDTH - 2)]{{DEFAULT_PLAYER_Y, DEFAULT_PLAYER_X}};

  unsigned short bodySize;
  char bodyChar;
  char colorScheme;
};

bool operator==(const Player::BodyNode&, const Player::BodyNode&);

#endif
