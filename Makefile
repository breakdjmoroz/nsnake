All: release debug profile

release: *.hpp 
	g++ -O1 -finline-functions *cpp -lncurses -o Nsnake

debug: *.hpp 
	g++ -g *.cpp -lncurses -o debug

profile: *.hpp
	g++ -pg	*.cpp -lncurses

