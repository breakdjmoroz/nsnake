#ifndef MENU
#define MENU

#include "ncurses.h"

const char* const MENU_STR= "MENU";
const char* const INFO_STR = "INFO";
const char* const EXIT_STR = "EXIT";

const char DEFAULT_MENU_HEIGHT = 3;
const char DEFAULT_MENU_WIDTH = 6;
const char DEFAULT_MENU_Y = 0;
const char DEFAULT_MENU_X = 0;

const char ENTER = 10;
const char DEFAULT_MENU_CHARACTER = 'm';

enum MENU_ITEMS: unsigned int
{
  ZERO,
  INFORM,
  EXIT
};

struct Menu
{
  WINDOW* window;
  MENU_ITEMS const choices[DEFAULT_MENU_HEIGHT - 1]{};

  char colorScheme;
  char subColorScheme;
};

Menu createMenu(const char, const char);
void updateMenu(const Menu&);
void destroyMenu(const Menu&);

void openMenu(const Menu&);
void closeMenu(const Menu&);

MENU_ITEMS select(const Menu&);

#endif

