#ifndef GAME
#define GAME

#include "ncurses.h"

#include "colorSchemes.hpp"

#include "scene.hpp"
#include "score.hpp"
#include "menu.hpp"
#include "infoPage.hpp"
#include "player.hpp"
#include "apple.hpp"
#include "mechanics.hpp"

#endif

